import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;
        System.out.print("Please input n: ");
        number = sc.nextInt();
        int i=1;
        while(i<number+1){
            int j=1;
            while(j<number+1){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
