import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String str;
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please input: ");
            str = sc.nextLine();
            if(str.trim().equalsIgnoreCase("bye")) { //trim isการตัดช่องว่างหน้าข้อความ
                System.out.println("Exit Program"); //ถ้าเจอbyeByeก็จะทำตรงนี้
                System.exit(0);
            }
            System.out.println(str);
        }
    }
}
