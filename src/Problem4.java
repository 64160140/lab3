import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;
        int sum = 0;
        int count = 0;

        do{
            System.out.print("Please input number: ");
            number = sc.nextInt();
            if(number!=0){
                sum = sum+number;
                count = count+1;
                System.out.println("sum: "+ sum + " Avg: " + (((double)sum)/count));
            }
        } while(number!=0);
        System.out.println("Bye");
    

        sc.close();
    }
}
