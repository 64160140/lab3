import java.util.Scanner;

public class Problem9For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;
        System.out.print("Please input n: ");
        number = sc.nextInt();
        for(int i=1; i<number+1; i++){
            for(int j=1; j<number+1; j++){
                System.out.print(j);
            }
            System.out.println();
        }
    }
}
